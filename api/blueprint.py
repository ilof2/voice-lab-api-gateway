from flask import Blueprint

api = Blueprint('api', __name__)


@api.route("/users/signout", methods=["DELETE", ])
def user_sign_out():
    pass


@api.route("/users/email/confirm", methods=["POST", ])
def user_email_confirm():
    pass


@api.route("/users/token/resend", methods=["POST", ])
def user_token_resend():
    pass


@api.route("/users/password/reset", methods=["POST", ])
def user_password_reset():
    pass


@api.route("/users/password/new", methods=["POST", ])
def user_password_new():
    pass


@api.route("/users/password/change", methods=["POST", ])
def user_password_change():
    pass


@api.route("/users/create", methods=["POST", ])
def user_create():
    pass


@api.route("/users/list/<limit>/<offset>", methods=["GET", ])
def users_list(limit, offset):
    pass


@api.route("/users/get/<uid>", methods=["GET", ])
def user_get(uid):
    pass


@api.route("/users/update/<uid>", methods=["PUT", ])
def user_update(uid):
    pass


@api.route("/users/delete/<uid>", methods=["DELETE", ])
def user_delete(uid):
    pass


@api.route("/users/check/admin/<uid>", methods=["GET", ])
def user_check_admin(uid):
    pass


@api.route("/users/identify", methods=["POST", ])
def user_identify():
    pass


@api.route("/users/identify/<uid>", methods=["GET", ])
def user_identify_by_id(uid):
    pass


