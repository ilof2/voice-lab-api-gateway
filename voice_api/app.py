from flask import Flask

from voice_api.config import Configuration


app = Flask(__name__)
app.config.from_object(Configuration)
