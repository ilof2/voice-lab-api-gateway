from api.blueprint import api
from .app import app

app.register_blueprint(users, url_prefix='/api')

if __name__ == "__main__":
    app.run()
